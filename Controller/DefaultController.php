<?php

namespace BedTech\Perseus\SaleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SaleBundle:Default:index.html.twig', array('name' => $name));
    }
}
