<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('sale_homepage', new Route('/hello/{name}', array(
    '_controller' => 'SaleBundle:Default:index',
)));

return $collection;
