<?php

namespace BedTech\Perseus\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SaleDetail
 */
class SaleDetail
{
    /**
     * @var integer
     */
    private $productId;

    /**
     * @var string
     */
    private $productNotes;

    /**
     * @var string
     */
    private $productUom;

    /**
     * @var \DateTime
     */
    private $scheduledDate;

    /**
     * @var string
     */
    private $quantite;

    /**
     * @var integer
     */
    private $priceListDetail;

    /**
     * @var integer
     */
    private $promotionDetail;

    /**
     * @var string
     */
    private $priceUnit;

    /**
     * @var string
     */
    private $taxTotal;

    /**
     * @var string
     */
    private $priceTotal;

    /**
     * @var string
     */
    private $priceFreight;

    /**
     * @var string
     */
    private $priceTotalDefcurr;

    /**
     * @var string
     */
    private $taxTotalDefcurr;

    /**
     * @var string
     */
    private $saleDetailNotes;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\Sale
     */
    private $SaleId;


    /**
     * Set productId
     *
     * @param integer $productId
     * @return SaleDetail
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set productNotes
     *
     * @param string $productNotes
     * @return SaleDetail
     */
    public function setProductNotes($productNotes)
    {
        $this->productNotes = $productNotes;

        return $this;
    }

    /**
     * Get productNotes
     *
     * @return string 
     */
    public function getProductNotes()
    {
        return $this->productNotes;
    }

    /**
     * Set productUom
     *
     * @param string $productUom
     * @return SaleDetail
     */
    public function setProductUom($productUom)
    {
        $this->productUom = $productUom;

        return $this;
    }

    /**
     * Get productUom
     *
     * @return string 
     */
    public function getProductUom()
    {
        return $this->productUom;
    }

    /**
     * Set scheduledDate
     *
     * @param \DateTime $scheduledDate
     * @return SaleDetail
     */
    public function setScheduledDate($scheduledDate)
    {
        $this->scheduledDate = $scheduledDate;

        return $this;
    }

    /**
     * Get scheduledDate
     *
     * @return \DateTime 
     */
    public function getScheduledDate()
    {
        return $this->scheduledDate;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     * @return SaleDetail
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string 
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set priceListDetail
     *
     * @param integer $priceListDetail
     * @return SaleDetail
     */
    public function setPriceListDetail($priceListDetail)
    {
        $this->priceListDetail = $priceListDetail;

        return $this;
    }

    /**
     * Get priceListDetail
     *
     * @return integer 
     */
    public function getPriceListDetail()
    {
        return $this->priceListDetail;
    }

    /**
     * Set promotionDetail
     *
     * @param integer $promotionDetail
     * @return SaleDetail
     */
    public function setPromotionDetail($promotionDetail)
    {
        $this->promotionDetail = $promotionDetail;

        return $this;
    }

    /**
     * Get promotionDetail
     *
     * @return integer 
     */
    public function getPromotionDetail()
    {
        return $this->promotionDetail;
    }

    /**
     * Set priceUnit
     *
     * @param string $priceUnit
     * @return SaleDetail
     */
    public function setPriceUnit($priceUnit)
    {
        $this->priceUnit = $priceUnit;

        return $this;
    }

    /**
     * Get priceUnit
     *
     * @return string 
     */
    public function getPriceUnit()
    {
        return $this->priceUnit;
    }

    /**
     * Set taxTotal
     *
     * @param string $taxTotal
     * @return SaleDetail
     */
    public function setTaxTotal($taxTotal)
    {
        $this->taxTotal = $taxTotal;

        return $this;
    }

    /**
     * Get taxTotal
     *
     * @return string 
     */
    public function getTaxTotal()
    {
        return $this->taxTotal;
    }

    /**
     * Set priceTotal
     *
     * @param string $priceTotal
     * @return SaleDetail
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;

        return $this;
    }

    /**
     * Get priceTotal
     *
     * @return string 
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * Set priceFreight
     *
     * @param string $priceFreight
     * @return SaleDetail
     */
    public function setPriceFreight($priceFreight)
    {
        $this->priceFreight = $priceFreight;

        return $this;
    }

    /**
     * Get priceFreight
     *
     * @return string 
     */
    public function getPriceFreight()
    {
        return $this->priceFreight;
    }

    /**
     * Set priceTotalDefcurr
     *
     * @param string $priceTotalDefcurr
     * @return SaleDetail
     */
    public function setPriceTotalDefcurr($priceTotalDefcurr)
    {
        $this->priceTotalDefcurr = $priceTotalDefcurr;

        return $this;
    }

    /**
     * Get priceTotalDefcurr
     *
     * @return string 
     */
    public function getPriceTotalDefcurr()
    {
        return $this->priceTotalDefcurr;
    }

    /**
     * Set taxTotalDefcurr
     *
     * @param string $taxTotalDefcurr
     * @return SaleDetail
     */
    public function setTaxTotalDefcurr($taxTotalDefcurr)
    {
        $this->taxTotalDefcurr = $taxTotalDefcurr;

        return $this;
    }

    /**
     * Get taxTotalDefcurr
     *
     * @return string 
     */
    public function getTaxTotalDefcurr()
    {
        return $this->taxTotalDefcurr;
    }

    /**
     * Set saleDetailNotes
     *
     * @param string $saleDetailNotes
     * @return SaleDetail
     */
    public function setSaleDetailNotes($saleDetailNotes)
    {
        $this->saleDetailNotes = $saleDetailNotes;

        return $this;
    }

    /**
     * Get saleDetailNotes
     *
     * @return string 
     */
    public function getSaleDetailNotes()
    {
        return $this->saleDetailNotes;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set SaleId
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\Sale $saleId
     * @return SaleDetail
     */
    public function setSaleId(\BedTech\Perseus\SaleBundle\Entity\Sale $saleId = null)
    {
        $this->SaleId = $saleId;

        return $this;
    }

    /**
     * Get SaleId
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\Sale 
     */
    public function getSaleId()
    {
        return $this->SaleId;
    }
}
