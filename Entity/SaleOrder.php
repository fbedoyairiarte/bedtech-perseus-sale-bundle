<?php

namespace BedTech\Perseus\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SaleOrder
 */
class SaleOrder
{
    /**
     * @var string
     */
    private $referenceOrder;

    /**
     * @var \DateTime
     */
    private $dateIssuedAt;

    /**
     * @var \DateTime
     */
    private $dateUpdatedAt;

    /**
     * @var \DateTime
     */
    private $dateDeliveryAt;

    /**
     * @var \DateTime
     */
    private $dateExpiresAt;

    /**
     * @var string
     */
    private $referencePayment;

    /**
     * @var string
     */
    private $orderSaleNotes;

    /**
     * @var string
     */
    private $total;

    /**
     * @var integer
     */
    private $billingAddressId;

    /**
     * @var integer
     */
    private $shippingAddressId;

    /**
     * @var \DateTime
     */
    private $dateCompletedAt;

    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BedTech\Core\CurrencyBundle\Entity\Currency
     */
    private $currency;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\PointSale
     */
    private $salesPoint;


    /**
     * Set referenceOrder
     *
     * @param string $referenceOrder
     * @return SaleOrder
     */
    public function setReferenceOrder($referenceOrder)
    {
        $this->referenceOrder = $referenceOrder;

        return $this;
    }

    /**
     * Get referenceOrder
     *
     * @return string 
     */
    public function getReferenceOrder()
    {
        return $this->referenceOrder;
    }

    /**
     * Set dateIssuedAt
     *
     * @param \DateTime $dateIssuedAt
     * @return SaleOrder
     */
    public function setDateIssuedAt($dateIssuedAt)
    {
        $this->dateIssuedAt = $dateIssuedAt;

        return $this;
    }

    /**
     * Get dateIssuedAt
     *
     * @return \DateTime 
     */
    public function getDateIssuedAt()
    {
        return $this->dateIssuedAt;
    }

    /**
     * Set dateUpdatedAt
     *
     * @param \DateTime $dateUpdatedAt
     * @return SaleOrder
     */
    public function setDateUpdatedAt($dateUpdatedAt)
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

    /**
     * Get dateUpdatedAt
     *
     * @return \DateTime 
     */
    public function getDateUpdatedAt()
    {
        return $this->dateUpdatedAt;
    }

    /**
     * Set dateDeliveryAt
     *
     * @param \DateTime $dateDeliveryAt
     * @return SaleOrder
     */
    public function setDateDeliveryAt($dateDeliveryAt)
    {
        $this->dateDeliveryAt = $dateDeliveryAt;

        return $this;
    }

    /**
     * Get dateDeliveryAt
     *
     * @return \DateTime 
     */
    public function getDateDeliveryAt()
    {
        return $this->dateDeliveryAt;
    }

    /**
     * Set dateExpiresAt
     *
     * @param \DateTime $dateExpiresAt
     * @return SaleOrder
     */
    public function setDateExpiresAt($dateExpiresAt)
    {
        $this->dateExpiresAt = $dateExpiresAt;

        return $this;
    }

    /**
     * Get dateExpiresAt
     *
     * @return \DateTime 
     */
    public function getDateExpiresAt()
    {
        return $this->dateExpiresAt;
    }

    /**
     * Set referencePayment
     *
     * @param string $referencePayment
     * @return SaleOrder
     */
    public function setReferencePayment($referencePayment)
    {
        $this->referencePayment = $referencePayment;

        return $this;
    }

    /**
     * Get referencePayment
     *
     * @return string 
     */
    public function getReferencePayment()
    {
        return $this->referencePayment;
    }

    /**
     * Set orderSaleNotes
     *
     * @param string $orderSaleNotes
     * @return SaleOrder
     */
    public function setOrderSaleNotes($orderSaleNotes)
    {
        $this->orderSaleNotes = $orderSaleNotes;

        return $this;
    }

    /**
     * Get orderSaleNotes
     *
     * @return string 
     */
    public function getOrderSaleNotes()
    {
        return $this->orderSaleNotes;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return SaleOrder
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set billingAddressId
     *
     * @param integer $billingAddressId
     * @return SaleOrder
     */
    public function setBillingAddressId($billingAddressId)
    {
        $this->billingAddressId = $billingAddressId;

        return $this;
    }

    /**
     * Get billingAddressId
     *
     * @return integer 
     */
    public function getBillingAddressId()
    {
        return $this->billingAddressId;
    }

    /**
     * Set shippingAddressId
     *
     * @param integer $shippingAddressId
     * @return SaleOrder
     */
    public function setShippingAddressId($shippingAddressId)
    {
        $this->shippingAddressId = $shippingAddressId;

        return $this;
    }

    /**
     * Get shippingAddressId
     *
     * @return integer 
     */
    public function getShippingAddressId()
    {
        return $this->shippingAddressId;
    }

    /**
     * Set dateCompletedAt
     *
     * @param \DateTime $dateCompletedAt
     * @return SaleOrder
     */
    public function setDateCompletedAt($dateCompletedAt)
    {
        $this->dateCompletedAt = $dateCompletedAt;

        return $this;
    }

    /**
     * Get dateCompletedAt
     *
     * @return \DateTime 
     */
    public function getDateCompletedAt()
    {
        return $this->dateCompletedAt;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     * @return SaleOrder
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string 
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currency
     *
     * @param \BedTech\Core\CurrencyBundle\Entity\Currency $currency
     * @return SaleOrder
     */
    public function setCurrency(\BedTech\Core\CurrencyBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \BedTech\Core\CurrencyBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set salesPoint
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\PointSale $salesPoint
     * @return SaleOrder
     */
    public function setSalesPoint(\BedTech\Perseus\SaleBundle\Entity\PointSale $salesPoint = null)
    {
        $this->salesPoint = $salesPoint;

        return $this;
    }

    /**
     * Get salesPoint
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\PointSale 
     */
    public function getSalesPoint()
    {
        return $this->salesPoint;
    }
}
