<?php

namespace BedTech\Perseus\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SalordDetail
 */
class SalordDetail
{
    /**
     * @var integer
     */
    private $productId;

    /**
     * @var string
     */
    private $productNotes;

    /**
     * @var string
     */
    private $productUom;

    /**
     * @var \DateTime
     */
    private $scheduledDate;

    /**
     * @var string
     */
    private $quantite;

    /**
     * @var integer
     */
    private $idPriceListDetail;

    /**
     * @var integer
     */
    private $idPromotionDetail;

    /**
     * @var string
     */
    private $priceUnite;

    /**
     * @var string
     */
    private $taxTotal;

    /**
     * @var string
     */
    private $priceTotal;

    /**
     * @var string
     */
    private $priceFreight;

    /**
     * @var string
     */
    private $priceTotalDefcurr;

    /**
     * @var string
     */
    private $taxTotalDefcurr;

    /**
     * @var integer
     */
    private $ordsaldetNotes;

    /**
     * @var integer
     */
    private $idOrdsaldet;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\SaleOrder
     */
    private $idSaleorder;


    /**
     * Set productId
     *
     * @param integer $productId
     * @return SalordDetail
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set productNotes
     *
     * @param string $productNotes
     * @return SalordDetail
     */
    public function setProductNotes($productNotes)
    {
        $this->productNotes = $productNotes;

        return $this;
    }

    /**
     * Get productNotes
     *
     * @return string 
     */
    public function getProductNotes()
    {
        return $this->productNotes;
    }

    /**
     * Set productUom
     *
     * @param string $productUom
     * @return SalordDetail
     */
    public function setProductUom($productUom)
    {
        $this->productUom = $productUom;

        return $this;
    }

    /**
     * Get productUom
     *
     * @return string 
     */
    public function getProductUom()
    {
        return $this->productUom;
    }

    /**
     * Set scheduledDate
     *
     * @param \DateTime $scheduledDate
     * @return SalordDetail
     */
    public function setScheduledDate($scheduledDate)
    {
        $this->scheduledDate = $scheduledDate;

        return $this;
    }

    /**
     * Get scheduledDate
     *
     * @return \DateTime 
     */
    public function getScheduledDate()
    {
        return $this->scheduledDate;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     * @return SalordDetail
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string 
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set idPriceListDetail
     *
     * @param integer $idPriceListDetail
     * @return SalordDetail
     */
    public function setIdPriceListDetail($idPriceListDetail)
    {
        $this->idPriceListDetail = $idPriceListDetail;

        return $this;
    }

    /**
     * Get idPriceListDetail
     *
     * @return integer 
     */
    public function getIdPriceListDetail()
    {
        return $this->idPriceListDetail;
    }

    /**
     * Set idPromotionDetail
     *
     * @param integer $idPromotionDetail
     * @return SalordDetail
     */
    public function setIdPromotionDetail($idPromotionDetail)
    {
        $this->idPromotionDetail = $idPromotionDetail;

        return $this;
    }

    /**
     * Get idPromotionDetail
     *
     * @return integer 
     */
    public function getIdPromotionDetail()
    {
        return $this->idPromotionDetail;
    }

    /**
     * Set priceUnite
     *
     * @param string $priceUnite
     * @return SalordDetail
     */
    public function setPriceUnite($priceUnite)
    {
        $this->priceUnite = $priceUnite;

        return $this;
    }

    /**
     * Get priceUnite
     *
     * @return string 
     */
    public function getPriceUnite()
    {
        return $this->priceUnite;
    }

    /**
     * Set taxTotal
     *
     * @param string $taxTotal
     * @return SalordDetail
     */
    public function setTaxTotal($taxTotal)
    {
        $this->taxTotal = $taxTotal;

        return $this;
    }

    /**
     * Get taxTotal
     *
     * @return string 
     */
    public function getTaxTotal()
    {
        return $this->taxTotal;
    }

    /**
     * Set priceTotal
     *
     * @param string $priceTotal
     * @return SalordDetail
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;

        return $this;
    }

    /**
     * Get priceTotal
     *
     * @return string 
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * Set priceFreight
     *
     * @param string $priceFreight
     * @return SalordDetail
     */
    public function setPriceFreight($priceFreight)
    {
        $this->priceFreight = $priceFreight;

        return $this;
    }

    /**
     * Get priceFreight
     *
     * @return string 
     */
    public function getPriceFreight()
    {
        return $this->priceFreight;
    }

    /**
     * Set priceTotalDefcurr
     *
     * @param string $priceTotalDefcurr
     * @return SalordDetail
     */
    public function setPriceTotalDefcurr($priceTotalDefcurr)
    {
        $this->priceTotalDefcurr = $priceTotalDefcurr;

        return $this;
    }

    /**
     * Get priceTotalDefcurr
     *
     * @return string 
     */
    public function getPriceTotalDefcurr()
    {
        return $this->priceTotalDefcurr;
    }

    /**
     * Set taxTotalDefcurr
     *
     * @param string $taxTotalDefcurr
     * @return SalordDetail
     */
    public function setTaxTotalDefcurr($taxTotalDefcurr)
    {
        $this->taxTotalDefcurr = $taxTotalDefcurr;

        return $this;
    }

    /**
     * Get taxTotalDefcurr
     *
     * @return string 
     */
    public function getTaxTotalDefcurr()
    {
        return $this->taxTotalDefcurr;
    }

    /**
     * Set ordsaldetNotes
     *
     * @param integer $ordsaldetNotes
     * @return SalordDetail
     */
    public function setOrdsaldetNotes($ordsaldetNotes)
    {
        $this->ordsaldetNotes = $ordsaldetNotes;

        return $this;
    }

    /**
     * Get ordsaldetNotes
     *
     * @return integer 
     */
    public function getOrdsaldetNotes()
    {
        return $this->ordsaldetNotes;
    }

    /**
     * Get idOrdsaldet
     *
     * @return integer 
     */
    public function getIdOrdsaldet()
    {
        return $this->idOrdsaldet;
    }

    /**
     * Set idSaleorder
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\SaleOrder $idSaleorder
     * @return SalordDetail
     */
    public function setIdSaleorder(\BedTech\Perseus\SaleBundle\Entity\SaleOrder $idSaleorder = null)
    {
        $this->idSaleorder = $idSaleorder;

        return $this;
    }

    /**
     * Get idSaleorder
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\SaleOrder 
     */
    public function getIdSaleorder()
    {
        return $this->idSaleorder;
    }
}
