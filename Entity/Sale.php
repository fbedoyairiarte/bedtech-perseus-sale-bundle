<?php

namespace BedTech\Perseus\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sale
 */
class Sale
{
    /**
     * @var string
     */
    private $referenceOrder;

    /**
     * @var string
     */
    private $invoceNumber;

    /**
     * @var integer
     */
    private $bookAddressD;

    /**
     * @var string
     */
    private $invoceCodeNumber;

    /**
     * @var integer
     */
    private $typeReferenceId;

    /**
     * @var \DateTime
     */
    private $dateSale;

    /**
     * @var string
     */
    private $typeSaleId;

    /**
     * @var boolean
     */
    private $deliveryStatus;

    /**
     * @var \DateTime
     */
    private $datePaymentLimit;

    /**
     * @var string
     */
    private $referencePayment;

    /**
     * @var integer
     */
    private $sellerId;

    /**
     * @var string
     */
    private $saleNet;

    /**
     * @var string
     */
    private $saleTotal;

    /**
     * @var string
     */
    private $saleTotalDefcurr;

    /**
     * @var string
     */
    private $currencyChange;

    /**
     * @var string
     */
    private $saleNotes;

    /**
     * @var string
     */
    private $saleGlosary;

    /**
     * @var integer
     */
    private $saleStatusId;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\Sale
     */
    private $idSalordReference;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\PointSale
     */
    private $pointSale;

    /**
     * @var \BedTech\Core\CurrencyBundle\Entity\Currency
     */
    private $currency;


    /**
     * Set referenceOrder
     *
     * @param string $referenceOrder
     * @return Sale
     */
    public function setReferenceOrder($referenceOrder)
    {
        $this->referenceOrder = $referenceOrder;

        return $this;
    }

    /**
     * Get referenceOrder
     *
     * @return string 
     */
    public function getReferenceOrder()
    {
        return $this->referenceOrder;
    }

    /**
     * Set invoceNumber
     *
     * @param string $invoceNumber
     * @return Sale
     */
    public function setInvoceNumber($invoceNumber)
    {
        $this->invoceNumber = $invoceNumber;

        return $this;
    }

    /**
     * Get invoceNumber
     *
     * @return string 
     */
    public function getInvoceNumber()
    {
        return $this->invoceNumber;
    }

    /**
     * Set bookAddressD
     *
     * @param integer $bookAddressD
     * @return Sale
     */
    public function setBookAddressD($bookAddressD)
    {
        $this->bookAddressD = $bookAddressD;

        return $this;
    }

    /**
     * Get bookAddressD
     *
     * @return integer 
     */
    public function getBookAddressD()
    {
        return $this->bookAddressD;
    }

    /**
     * Set invoceCodeNumber
     *
     * @param string $invoceCodeNumber
     * @return Sale
     */
    public function setInvoceCodeNumber($invoceCodeNumber)
    {
        $this->invoceCodeNumber = $invoceCodeNumber;

        return $this;
    }

    /**
     * Get invoceCodeNumber
     *
     * @return string 
     */
    public function getInvoceCodeNumber()
    {
        return $this->invoceCodeNumber;
    }

    /**
     * Set typeReferenceId
     *
     * @param integer $typeReferenceId
     * @return Sale
     */
    public function setTypeReferenceId($typeReferenceId)
    {
        $this->typeReferenceId = $typeReferenceId;

        return $this;
    }

    /**
     * Get typeReferenceId
     *
     * @return integer 
     */
    public function getTypeReferenceId()
    {
        return $this->typeReferenceId;
    }

    /**
     * Set dateSale
     *
     * @param \DateTime $dateSale
     * @return Sale
     */
    public function setDateSale($dateSale)
    {
        $this->dateSale = $dateSale;

        return $this;
    }

    /**
     * Get dateSale
     *
     * @return \DateTime 
     */
    public function getDateSale()
    {
        return $this->dateSale;
    }

    /**
     * Set typeSaleId
     *
     * @param string $typeSaleId
     * @return Sale
     */
    public function setTypeSaleId($typeSaleId)
    {
        $this->typeSaleId = $typeSaleId;

        return $this;
    }

    /**
     * Get typeSaleId
     *
     * @return string 
     */
    public function getTypeSaleId()
    {
        return $this->typeSaleId;
    }

    /**
     * Set deliveryStatus
     *
     * @param boolean $deliveryStatus
     * @return Sale
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return boolean 
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * Set datePaymentLimit
     *
     * @param \DateTime $datePaymentLimit
     * @return Sale
     */
    public function setDatePaymentLimit($datePaymentLimit)
    {
        $this->datePaymentLimit = $datePaymentLimit;

        return $this;
    }

    /**
     * Get datePaymentLimit
     *
     * @return \DateTime 
     */
    public function getDatePaymentLimit()
    {
        return $this->datePaymentLimit;
    }

    /**
     * Set referencePayment
     *
     * @param string $referencePayment
     * @return Sale
     */
    public function setReferencePayment($referencePayment)
    {
        $this->referencePayment = $referencePayment;

        return $this;
    }

    /**
     * Get referencePayment
     *
     * @return string 
     */
    public function getReferencePayment()
    {
        return $this->referencePayment;
    }

    /**
     * Set sellerId
     *
     * @param integer $sellerId
     * @return Sale
     */
    public function setSellerId($sellerId)
    {
        $this->sellerId = $sellerId;

        return $this;
    }

    /**
     * Get sellerId
     *
     * @return integer 
     */
    public function getSellerId()
    {
        return $this->sellerId;
    }

    /**
     * Set saleNet
     *
     * @param string $saleNet
     * @return Sale
     */
    public function setSaleNet($saleNet)
    {
        $this->saleNet = $saleNet;

        return $this;
    }

    /**
     * Get saleNet
     *
     * @return string 
     */
    public function getSaleNet()
    {
        return $this->saleNet;
    }

    /**
     * Set saleTotal
     *
     * @param string $saleTotal
     * @return Sale
     */
    public function setSaleTotal($saleTotal)
    {
        $this->saleTotal = $saleTotal;

        return $this;
    }

    /**
     * Get saleTotal
     *
     * @return string 
     */
    public function getSaleTotal()
    {
        return $this->saleTotal;
    }

    /**
     * Set saleTotalDefcurr
     *
     * @param string $saleTotalDefcurr
     * @return Sale
     */
    public function setSaleTotalDefcurr($saleTotalDefcurr)
    {
        $this->saleTotalDefcurr = $saleTotalDefcurr;

        return $this;
    }

    /**
     * Get saleTotalDefcurr
     *
     * @return string 
     */
    public function getSaleTotalDefcurr()
    {
        return $this->saleTotalDefcurr;
    }

    /**
     * Set currencyChange
     *
     * @param string $currencyChange
     * @return Sale
     */
    public function setCurrencyChange($currencyChange)
    {
        $this->currencyChange = $currencyChange;

        return $this;
    }

    /**
     * Get currencyChange
     *
     * @return string 
     */
    public function getCurrencyChange()
    {
        return $this->currencyChange;
    }

    /**
     * Set saleNotes
     *
     * @param string $saleNotes
     * @return Sale
     */
    public function setSaleNotes($saleNotes)
    {
        $this->saleNotes = $saleNotes;

        return $this;
    }

    /**
     * Get saleNotes
     *
     * @return string 
     */
    public function getSaleNotes()
    {
        return $this->saleNotes;
    }

    /**
     * Set saleGlosary
     *
     * @param string $saleGlosary
     * @return Sale
     */
    public function setSaleGlosary($saleGlosary)
    {
        $this->saleGlosary = $saleGlosary;

        return $this;
    }

    /**
     * Get saleGlosary
     *
     * @return string 
     */
    public function getSaleGlosary()
    {
        return $this->saleGlosary;
    }

    /**
     * Set saleStatusId
     *
     * @param integer $saleStatusId
     * @return Sale
     */
    public function setSaleStatusId($saleStatusId)
    {
        $this->saleStatusId = $saleStatusId;

        return $this;
    }

    /**
     * Get saleStatusId
     *
     * @return integer 
     */
    public function getSaleStatusId()
    {
        return $this->saleStatusId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idSalordReference
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\Sale $idSalordReference
     * @return Sale
     */
    public function setIdSalordReference(\BedTech\Perseus\SaleBundle\Entity\Sale $idSalordReference = null)
    {
        $this->idSalordReference = $idSalordReference;

        return $this;
    }

    /**
     * Get idSalordReference
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\Sale 
     */
    public function getIdSalordReference()
    {
        return $this->idSalordReference;
    }

    /**
     * Set pointSale
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\PointSale $pointSale
     * @return Sale
     */
    public function setPointSale(\BedTech\Perseus\SaleBundle\Entity\PointSale $pointSale = null)
    {
        $this->pointSale = $pointSale;

        return $this;
    }

    /**
     * Get pointSale
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\PointSale 
     */
    public function getPointSale()
    {
        return $this->pointSale;
    }

    /**
     * Set currency
     *
     * @param \BedTech\Core\CurrencyBundle\Entity\Currency $currency
     * @return Sale
     */
    public function setCurrency(\BedTech\Core\CurrencyBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \BedTech\Core\CurrencyBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
