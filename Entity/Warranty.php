<?php

namespace BedTech\Perseus\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Warranty
 */
class Warranty
{
    /**
     * @var \DateTime
     */
    private $dateStart;

    /**
     * @var \DateTime
     */
    private $dateEnd;

    /**
     * @var string
     */
    private $conditions;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \BedTech\Perseus\SaleBundle\Entity\SaleDetail
     */
    private $saleDetailId;


    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return Warranty
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return Warranty
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set conditions
     *
     * @param string $conditions
     * @return Warranty
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * Get conditions
     *
     * @return string 
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Warranty
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set saleDetailId
     *
     * @param \BedTech\Perseus\SaleBundle\Entity\SaleDetail $saleDetailId
     * @return Warranty
     */
    public function setSaleDetailId(\BedTech\Perseus\SaleBundle\Entity\SaleDetail $saleDetailId = null)
    {
        $this->saleDetailId = $saleDetailId;

        return $this;
    }

    /**
     * Get saleDetailId
     *
     * @return \BedTech\Perseus\SaleBundle\Entity\SaleDetail 
     */
    public function getSaleDetailId()
    {
        return $this->saleDetailId;
    }
}
